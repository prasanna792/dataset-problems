const data = [
    {
        "aircraftType": "regional",
        "airline": {
            "name": "SriLankan Airlines",
            "iataCode": "UL"
        },
        "airplane": {
            "name": "Antonov An-72",
            "iataTypeCode": "AN7"
        },
        "passengers": [
            {
                "fullName": "Darrel Heaney",
                "jobArea": "Applications",
                "zodiacSign": "Gemini",
                "company": "Wyman and Sons",
                "address": {
                    "streetAddress": "56679 Christian River",
                    "zipCode": "72591-2968",
                    "city": "Reinholdton",
                    "country": "Christmas Island"
                },
                "accountNumber": "78644551",
                "currency": {
                    "name": "Pula",
                    "code": "BWP",
                    "symbol": "P"
                },
                "currencyCode": "DOP",
                "salary": "196.74",
                "experience": "2"
            },
            {
                "fullName": "Candace Schuppe",
                "jobArea": "Usability",
                "zodiacSign": "Scorpio",
                "company": "Kemmer and Sons",
                "address": {
                    "streetAddress": "99720 Sipes Causeway",
                    "zipCode": "02542",
                    "city": "Palm Bay",
                    "country": "Philippines"
                },
                "accountNumber": "53955630",
                "currency": {
                    "name": "Saudi Riyal",
                    "code": "SAR",
                    "symbol": "﷼"
                },
                "currencyCode": "SRD",
                "salary": "698.98",
                "experience": "6"
            }
        ],
        "captain": {
            "fullName": "Mr. Cecil Jakubowski MD",
            "jobArea": "Tactics",
            "zodiacSign": "Scorpio",
            "company": "Abbott - Prohaska",
            "address": {
                "streetAddress": "973 Iliana Coves",
                "zipCode": "38383-4754",
                "city": "Troy",
                "country": "Nigeria"
            },
            "accountNumber": "18655620",
            "currency": {
                "name": "New Zealand Dollar",
                "code": "NZD",
                "symbol": "$"
            },
            "currencyCode": "SDG",
            "salary": "136.40",
            "experience": "8"
        }
    },
    {
        "aircraftType": "narrowbody",
        "airline": {
            "name": "S7 Airlines",
            "iataCode": "S7"
        },
        "airplane": {
            "name": "Ilyushin IL96-300",
            "iataTypeCode": "I93"
        },
        "passengers": [
            {
                "fullName": "Karl Jerde MD",
                "jobArea": "Marketing",
                "zodiacSign": "Libra",
                "company": "Ryan LLC",
                "address": {
                    "streetAddress": "637 Nitzsche Station",
                    "zipCode": "97873",
                    "city": "West Mose",
                    "country": "Albania"
                },
                "accountNumber": "14416949",
                "currency": {
                    "name": "Uzbekistan Sum",
                    "code": "UZS",
                    "symbol": "лв"
                },
                "currencyCode": "KZT",
                "salary": "829.80",
                "experience": "2"
            },
            {
                "fullName": "Mrs. Joann Larson",
                "jobArea": "Mobility",
                "zodiacSign": "Virgo",
                "company": "Lind, Emmerich and Altenwerth",
                "address": {
                    "streetAddress": "438 Wisozk Lodge",
                    "zipCode": "97805-9594",
                    "city": "Hauckland",
                    "country": "South Africa"
                },
                "accountNumber": "37005826",
                "currency": {
                    "name": "Australian Dollar",
                    "code": "AUD",
                    "symbol": "$"
                },
                "currencyCode": "HNL",
                "salary": "472.92",
                "experience": "6"
            }
        ],
        "captain": {
            "fullName": "Holly Rolfson I",
            "jobArea": "Group",
            "zodiacSign": "Libra",
            "company": "Kirlin - Rice",
            "address": {
                "streetAddress": "5101 Aletha Drives",
                "zipCode": "38560-8723",
                "city": "North Delbertshire",
                "country": "Uganda"
            },
            "accountNumber": "87430902",
            "currency": {
                "name": "Dobra",
                "code": "STN",
                "symbol": "Db"
            },
            "currencyCode": "KYD",
            "salary": "226.13",
            "experience": "4"
        }
    },
    {
        "aircraftType": "regional",
        "airline": {
            "name": "VOEPASS Linhas Aereas",
            "iataCode": "2Z"
        },
        "airplane": {
            "name": "Airbus A310-300",
            "iataTypeCode": "313"
        },
        "passengers": [
            {
                "fullName": "Julia Pfeffer Sr.",
                "jobArea": "Interactions",
                "zodiacSign": "Taurus",
                "company": "Franey Group",
                "address": {
                    "streetAddress": "764 Enrico Street",
                    "zipCode": "06382-5080",
                    "city": "New Abnerside",
                    "country": "Netherlands"
                },
                "accountNumber": "36329816",
                "currency": {
                    "name": "Convertible Marks",
                    "code": "BAM",
                    "symbol": "KM"
                },
                "currencyCode": "KHR",
                "salary": "191.95",
                "experience": "4"
            },
            {
                "fullName": "Josefina Jacobi",
                "jobArea": "Markets",
                "zodiacSign": "Sagittarius",
                "company": "Bartell LLC",
                "address": {
                    "streetAddress": "39256 Schneider Lane",
                    "zipCode": "19034-7073",
                    "city": "Rossview",
                    "country": "Mauritania"
                },
                "accountNumber": "09325345",
                "currency": {
                    "name": "Cedi",
                    "code": "GHS",
                    "symbol": ""
                },
                "currencyCode": "TOP",
                "salary": "578.36",
                "experience": "4"
            }
        ],
        "captain": {
            "fullName": "Vanessa Runte",
            "jobArea": "Identity",
            "zodiacSign": "Cancer",
            "company": "Hermiston Inc",
            "address": {
                "streetAddress": "5383 Marvin Pines",
                "zipCode": "58905-1214",
                "city": "Lake Augustland",
                "country": "Maldives"
            },
            "accountNumber": "48009090",
            "currency": {
                "name": "Gourde",
                "code": "HTG",
                "symbol": ""
            },
            "currencyCode": "TTD",
            "salary": "32.20",
            "experience": "8"
        }
    },
    {
        "aircraftType": "narrowbody",
        "airline": {
            "name": "LATAM Airlines",
            "iataCode": "LA"
        },
        "airplane": {
            "name": "Antonov An-148",
            "iataTypeCode": "A81"
        },
        "passengers": [
            {
                "fullName": "Irma Boehm",
                "jobArea": "Identity",
                "zodiacSign": "Libra",
                "company": "Jenkins - Dickens",
                "address": {
                    "streetAddress": "55053 Rohan Avenue",
                    "zipCode": "64190-9399",
                    "city": "Ewellworth",
                    "country": "Philippines"
                },
                "accountNumber": "17185838",
                "currency": {
                    "name": "Argentine Peso",
                    "code": "ARS",
                    "symbol": "$"
                },
                "currencyCode": "AFN",
                "salary": "300.33",
                "experience": "3"
            },
            {
                "fullName": "Leticia Towne",
                "jobArea": "Quality",
                "zodiacSign": "Libra",
                "company": "Marquardt Inc",
                "address": {
                    "streetAddress": "873 Corbin Rue",
                    "zipCode": "28445-3775",
                    "city": "Lake Sophie",
                    "country": "Iceland"
                },
                "accountNumber": "62900355",
                "currency": {
                    "name": "Guinea Franc",
                    "code": "GNF",
                    "symbol": ""
                },
                "currencyCode": "GYD",
                "salary": "658.18",
                "experience": "3"
            }
        ],
        "captain": {
            "fullName": "Diana Durgan II",
            "jobArea": "Accountability",
            "zodiacSign": "Libra",
            "company": "Cartwright - Gutmann",
            "address": {
                "streetAddress": "903 Botsford Shoals",
                "zipCode": "06062-7777",
                "city": "Davisbury",
                "country": "Papua New Guinea"
            },
            "accountNumber": "53685825",
            "currency": {
                "name": "Pataca",
                "code": "MOP",
                "symbol": ""
            },
            "currencyCode": "RWF",
            "salary": "63.99",
            "experience": "4"
        }
    },
    {
        "aircraftType": "widebody",
        "airline": {
            "name": "TUI Airways",
            "iataCode": "BY"
        },
        "airplane": {
            "name": "Antonov An-30",
            "iataTypeCode": "A30"
        },
        "passengers": [
            {
                "fullName": "Dr. Lydia Block-Grant",
                "jobArea": "Web",
                "zodiacSign": "Sagittarius",
                "company": "Leannon Group",
                "address": {
                    "streetAddress": "367 Anderson Extension",
                    "zipCode": "18258",
                    "city": "Rennerhaven",
                    "country": "Zimbabwe"
                },
                "accountNumber": "11514283",
                "currency": {
                    "name": "Argentine Peso",
                    "code": "ARS",
                    "symbol": "$"
                },
                "currencyCode": "MXN",
                "salary": "525.65",
                "experience": "5"
            },
            {
                "fullName": "Sheri Schinner",
                "jobArea": "Intranet",
                "zodiacSign": "Virgo",
                "company": "Sanford - Schinner",
                "address": {
                    "streetAddress": "118 Emie Mews",
                    "zipCode": "12480-7320",
                    "city": "Terrencemouth",
                    "country": "Mauritius"
                },
                "accountNumber": "57256710",
                "currency": {
                    "name": "Barbados Dollar",
                    "code": "BBD",
                    "symbol": "$"
                },
                "currencyCode": "FJD",
                "salary": "834.76",
                "experience": "8"
            }
        ],
        "captain": {
            "fullName": "Armando Schmitt",
            "jobArea": "Web",
            "zodiacSign": "Leo",
            "company": "King - Marks",
            "address": {
                "streetAddress": "3943 Kailey Keys",
                "zipCode": "86455-8822",
                "city": "Suffolk",
                "country": "Uzbekistan"
            },
            "accountNumber": "07998107",
            "currency": {
                "name": "Vatu",
                "code": "VUV",
                "symbol": ""
            },
            "currencyCode": "AFN",
            "salary": "533.60",
            "experience": "8"
        }
    },
    {
        "aircraftType": "narrowbody",
        "airline": {
            "name": "Juneyao Airlines",
            "iataCode": "HO"
        },
        "airplane": {
            "name": "Ilyushin IL114",
            "iataTypeCode": "I14"
        },
        "passengers": [
            {
                "fullName": "Mack Harber",
                "jobArea": "Operations",
                "zodiacSign": "Pisces",
                "company": "Cummings, Mohr and Halvorson",
                "address": {
                    "streetAddress": "5540 Rutherford Canyon",
                    "zipCode": "87942",
                    "city": "Dayneborough",
                    "country": "Zambia"
                },
                "accountNumber": "26071358",
                "currency": {
                    "name": "Danish Krone",
                    "code": "DKK",
                    "symbol": "kr"
                },
                "currencyCode": "TND",
                "salary": "470.71",
                "experience": "8"
            },
            {
                "fullName": "Claire Hahn",
                "jobArea": "Infrastructure",
                "zodiacSign": "Libra",
                "company": "Kautzer - Batz",
                "address": {
                    "streetAddress": "686 Osinski Spurs",
                    "zipCode": "13589",
                    "city": "East Kavon",
                    "country": "Somalia"
                },
                "accountNumber": "68820124",
                "currency": {
                    "name": "Bermudian Dollar (customarily known as Bermuda Dollar)",
                    "code": "BMD",
                    "symbol": "$"
                },
                "currencyCode": "JMD",
                "salary": "860.16",
                "experience": "2"
            }
        ],
        "captain": {
            "fullName": "Jimmie Roberts",
            "jobArea": "Identity",
            "zodiacSign": "Scorpio",
            "company": "Terry - Satterfield",
            "address": {
                "streetAddress": "57657 Elijah Burgs",
                "zipCode": "95644-2861",
                "city": "North Demetris",
                "country": "Paraguay"
            },
            "accountNumber": "51812120",
            "currency": {
                "name": "Jamaican Dollar",
                "code": "JMD",
                "symbol": "J$"
            },
            "currencyCode": "GMD",
            "salary": "25.32",
            "experience": "7"
        }
    },
    {
        "aircraftType": "regional",
        "airline": {
            "name": "Philippine Airlines",
            "iataCode": "PR"
        },
        "airplane": {
            "name": "Airbus A300",
            "iataTypeCode": "AB3"
        },
        "passengers": [
            {
                "fullName": "Betsy Bernier",
                "jobArea": "Creative",
                "zodiacSign": "Scorpio",
                "company": "Muller, Ledner and Kuhic",
                "address": {
                    "streetAddress": "1956 Murazik Harbor",
                    "zipCode": "65352-9299",
                    "city": "Mikelside",
                    "country": "Belize"
                },
                "accountNumber": "33295425",
                "currency": {
                    "name": "Kwacha",
                    "code": "MWK",
                    "symbol": ""
                },
                "currencyCode": "ARS",
                "salary": "481.19",
                "experience": "1"
            },
            {
                "fullName": "Perry Abernathy",
                "jobArea": "Creative",
                "zodiacSign": "Aries",
                "company": "Heathcote - Green",
                "address": {
                    "streetAddress": "498 Ratke Ports",
                    "zipCode": "39336-8986",
                    "city": "New Rozella",
                    "country": "Libyan Arab Jamahiriya"
                },
                "accountNumber": "15805926",
                "currency": {
                    "name": "Belarusian Ruble",
                    "code": "BYN",
                    "symbol": "Rbl"
                },
                "currencyCode": "CRC",
                "salary": "805.83",
                "experience": "0"
            }
        ],
        "captain": {
            "fullName": "Fernando Johnson",
            "jobArea": "Accounts",
            "zodiacSign": "Leo",
            "company": "Abernathy and Sons",
            "address": {
                "streetAddress": "449 Monserrat Club",
                "zipCode": "41959",
                "city": "West Claudie",
                "country": "Israel"
            },
            "accountNumber": "67761623",
            "currency": {
                "name": "Namibia Dollar",
                "code": "NAD",
                "symbol": "N$"
            },
            "currencyCode": "AMD",
            "salary": "37.50",
            "experience": "7"
        }
    },
    {
        "aircraftType": "narrowbody",
        "airline": {
            "name": "Jet2",
            "iataCode": "LS"
        },
        "airplane": {
            "name": "Airbus A321neo",
            "iataTypeCode": "32Q"
        },
        "passengers": [
            {
                "fullName": "Dr. Danny Hagenes",
                "jobArea": "Group",
                "zodiacSign": "Leo",
                "company": "Yundt - Vandervort",
                "address": {
                    "streetAddress": "72238 Gaetano Locks",
                    "zipCode": "14414",
                    "city": "Halbury",
                    "country": "Portugal"
                },
                "accountNumber": "09673794",
                "currency": {
                    "name": "East Caribbean Dollar",
                    "code": "XCD",
                    "symbol": "$"
                },
                "currencyCode": "GTQ",
                "salary": "848.01",
                "experience": "8"
            },
            {
                "fullName": "Stella Swift IV",
                "jobArea": "Program",
                "zodiacSign": "Libra",
                "company": "Kutch and Sons",
                "address": {
                    "streetAddress": "90690 Cassie Loaf",
                    "zipCode": "29596",
                    "city": "New Hollis",
                    "country": "Uruguay"
                },
                "accountNumber": "29774141",
                "currency": {
                    "name": "Saint Helena Pound",
                    "code": "SHP",
                    "symbol": "£"
                },
                "currencyCode": "XPF",
                "salary": "848.31",
                "experience": "1"
            }
        ],
        "captain": {
            "fullName": "Justin Runte",
            "jobArea": "Accountability",
            "zodiacSign": "Cancer",
            "company": "Goodwin, Marvin and Smitham",
            "address": {
                "streetAddress": "59243 Raynor Dale",
                "zipCode": "25797-2819",
                "city": "Kailynview",
                "country": "Kyrgyz Republic"
            },
            "accountNumber": "34317410",
            "currency": {
                "name": "Guarani",
                "code": "PYG",
                "symbol": "Gs"
            },
            "currencyCode": "ZWL",
            "salary": "86.08",
            "experience": "1"
        }
    },
    {
        "aircraftType": "narrowbody",
        "airline": {
            "name": "Ural Airlines",
            "iataCode": "U6"
        },
        "airplane": {
            "name": "Boeing 717",
            "iataTypeCode": "717"
        },
        "passengers": [
            {
                "fullName": "Miriam Hessel-Shanahan",
                "jobArea": "Metrics",
                "zodiacSign": "Leo",
                "company": "Lockman, Farrell and Mills",
                "address": {
                    "streetAddress": "55979 Amparo Heights",
                    "zipCode": "47789",
                    "city": "North Henriette",
                    "country": "Estonia"
                },
                "accountNumber": "71987644",
                "currency": {
                    "name": "Qatari Rial",
                    "code": "QAR",
                    "symbol": "﷼"
                },
                "currencyCode": "GBP",
                "salary": "111.91",
                "experience": "8"
            },
            {
                "fullName": "Ebony Bailey",
                "jobArea": "Web",
                "zodiacSign": "Leo",
                "company": "Champlin, Haley and Pollich",
                "address": {
                    "streetAddress": "23230 Ike Harbors",
                    "zipCode": "76061",
                    "city": "Fort Winifred",
                    "country": "Norfolk Island"
                },
                "accountNumber": "22061112",
                "currency": {
                    "name": "South Sudanese pound",
                    "code": "SSP",
                    "symbol": ""
                },
                "currencyCode": "PGK",
                "salary": "436.01",
                "experience": "0"
            }
        ],
        "captain": {
            "fullName": "Dominic Huel",
            "jobArea": "Optimization",
            "zodiacSign": "Leo",
            "company": "Witting Inc",
            "address": {
                "streetAddress": "29464 Michelle Isle",
                "zipCode": "77183-5513",
                "city": "Montyburgh",
                "country": "New Zealand"
            },
            "accountNumber": "88500184",
            "currency": {
                "name": "Guyana Dollar",
                "code": "GYD",
                "symbol": "$"
            },
            "currencyCode": "MUR",
            "salary": "370.14",
            "experience": "9"
        }
    },
    {
        "aircraftType": "regional",
        "airline": {
            "name": "Fiji Airways",
            "iataCode": "FJ"
        },
        "airplane": {
            "name": "Embraer 170",
            "iataTypeCode": "E70"
        },
        "passengers": [
            {
                "fullName": "Dr. Carl Harvey II",
                "jobArea": "Tactics",
                "zodiacSign": "Capricorn",
                "company": "Williamson, Jacobi and Williamson",
                "address": {
                    "streetAddress": "87907 Maggio Track",
                    "zipCode": "16349",
                    "city": "Kendallstad",
                    "country": "Jersey"
                },
                "accountNumber": "49414671",
                "currency": {
                    "name": "Bermudian Dollar (customarily known as Bermuda Dollar)",
                    "code": "BMD",
                    "symbol": "$"
                },
                "currencyCode": "ISK",
                "salary": "143.78",
                "experience": "8"
            },
            {
                "fullName": "Rosemarie Barton",
                "jobArea": "Web",
                "zodiacSign": "Aries",
                "company": "Jerde, Von and Schaden",
                "address": {
                    "streetAddress": "40445 Schinner Rue",
                    "zipCode": "73440-0177",
                    "city": "Daly City",
                    "country": "Poland"
                },
                "accountNumber": "14917062",
                "currency": {
                    "name": "Tugrik",
                    "code": "MNT",
                    "symbol": "₮"
                },
                "currencyCode": "JMD",
                "salary": "663.02",
                "experience": "5"
            }
        ],
        "captain": {
            "fullName": "Catherine Wisoky",
            "jobArea": "Operations",
            "zodiacSign": "Scorpio",
            "company": "Abbott, Walker and Weimann",
            "address": {
                "streetAddress": "6960 Mekhi Isle",
                "zipCode": "72300-5362",
                "city": "East Rachelle",
                "country": "Cameroon"
            },
            "accountNumber": "36058446",
            "currency": {
                "name": "Uzbekistan Sum",
                "code": "UZS",
                "symbol": "лв"
            },
            "currencyCode": "PLN",
            "salary": "677.00",
            "experience": "2"
        }
    },
    {
        "aircraftType": "widebody",
        "airline": {
            "name": "Star Peru",
            "iataCode": "2I"
        },
        "airplane": {
            "name": "McDonnell Douglas MD11",
            "iataTypeCode": "M11"
        },
        "passengers": [
            {
                "fullName": "Cecil Shields III",
                "jobArea": "Division",
                "zodiacSign": "Virgo",
                "company": "Runolfsson, Cole and Zulauf",
                "address": {
                    "streetAddress": "2389 Schamberger Fork",
                    "zipCode": "72982",
                    "city": "Lake Lizziestad",
                    "country": "Ghana"
                },
                "accountNumber": "19623279",
                "currency": {
                    "name": "Solomon Islands Dollar",
                    "code": "SBD",
                    "symbol": "$"
                },
                "currencyCode": "GYD",
                "salary": "909.30",
                "experience": "5"
            },
            {
                "fullName": "Christine Nader",
                "jobArea": "Branding",
                "zodiacSign": "Taurus",
                "company": "Spencer Group",
                "address": {
                    "streetAddress": "2869 Ole Neck",
                    "zipCode": "63980-6673",
                    "city": "Schillerville",
                    "country": "Kuwait"
                },
                "accountNumber": "71565345",
                "currency": {
                    "name": "CFA Franc BEAC",
                    "code": "XAF",
                    "symbol": ""
                },
                "currencyCode": "GYD",
                "salary": "281.59",
                "experience": "0"
            }
        ],
        "captain": {
            "fullName": "Theodore Oberbrunner",
            "jobArea": "Integration",
            "zodiacSign": "Taurus",
            "company": "Thompson Inc",
            "address": {
                "streetAddress": "28625 Huels Trail",
                "zipCode": "30101-0126",
                "city": "Cheektowaga",
                "country": "Mexico"
            },
            "accountNumber": "68067098",
            "currency": {
                "name": "Nepalese Rupee",
                "code": "NPR",
                "symbol": "₨"
            },
            "currencyCode": "UZS",
            "salary": "351.65",
            "experience": "9"
        }
    }
]

/* 
    1. Group the data based on aircraft type using only one HOF
    2. Sort the data in descending order of name
    3. Sort the data in ascending order of iata type code
    4. Split the full name into it's component parts and store each as a new key in the object
    5. Group the data based on zodiac sign using only one HOF
    6. Split the street address into it's component parts and store each as a new key in the object
    7. Group the data based on city using only one HOF
    8. Group the data based on country using only one HOF
    9. Convert the account number into the correct numeric equivalent.
    10. Use the currency code key to make a more readable version with salary
    11. Find the total salary
    12. Group the data based on experience using only one HOF
    13. Flatten the currency object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.
    14. Convert the salary into the correct numeric equivalent.
    15. Find the average salary grouped by experience
    16. Flatten the airline object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.
    17. Flatten the airplane object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.
    18. Sort the data in descending order of code
    19. Use the experience as a factor to calculate the factored salary
    20. Sort the data in ascending order of name
    21. Find the average salary
    22. Sort the data in descending order of iata type code
    23. Sort the data in descending order of iata code
    24. Sort the data in ascending order of code

    NOTE: Save the name of this file as generated-data-for-hof.cjs

*/


// 1. Group the data based on aircraft type using only one HOF

let aircraftTypes = data.reduce((accumulatar, current) => {

    if (accumulatar[current.aircraftType]) {
        accumulatar[current.aircraftType].push(current)
    } else {
        accumulatar[current.aircraftType] = [];
        accumulatar[current.aircraftType].push(current)
    }
    return accumulatar;

}, {})

// console.log(JSON.stringify(aircraftTypes,null,2));



// 2. Sort the data in descending order of name

let descendingOrderOfName = data.sort((item1, item2) => {
    if (item1.airline.name < item2.airline.name) {
        return 1
    } else if (item1.airline.name > item2.airline.name) {
        return -1;
    } else {
        return 0;
    }
});

// console.log(descendingOrderOfName)


// 3. Sort the data in ascending order of iata type code


let iataCodeType = data.sort((item1, item2) => {
    if (item1.airplane.iataTypeCode > item2.airplane.iataTypeCode) {
        return 1;
    } else if (item1.airplane.iataTypeCode < item2.airplane.iataTypeCode) {
        return -1;
    } else {
        return 0;
    }
});

// console.log(iataCodeType);


// 4. Split the full name into it's component parts and store each as a new key in the object

const splitFullNameOfCaptain = data.reduce((acc, item) => {

    const [captainFirstName, captainLastName] = item.captain.fullName.split(" ");
    item.captain.firstName = captainFirstName;
    item.captain.lastName = captainLastName;

    const updatedPassengers = item.passengers.map((passenger) => {
        const [passengerFirstName, passengerLastName] = passenger.fullName.split(" ");
        passenger.firstName = passengerFirstName;
        passenger.lastName = passengerLastName;
        return passenger;

    });

    acc.push({
        ...item,
        passengers: updatedPassengers
    });

    return acc;
}, []);

//   console.log(JSON.stringify(splitFullNameOfCaptain,null,2));

// console.log(splitFullNameOfCaptain);


// 5. Group the data based on zodiac sign using only one HOF


const zodiacSign = data.reduce((result, item) => {
    const { zodiacSign } = item.captain;

    if (!result.captains[zodiacSign]) {
        result.captains[zodiacSign] = [item.captain];
    } else {
        result.captains[zodiacSign].push(item.captain);
    }

    item.passengers.reduce((passengers, passenger) => {
        const { zodiacSign } = passenger;
        if (!passengers[zodiacSign]) {
            passengers[zodiacSign] = [passenger];
        } else {
            passengers[zodiacSign].push(passenger);
        }
        return passengers;
    }, result.passengers);

    return result;
}, { captains: {}, passengers: {} });

//   console.log(JSON.stringify(zodiacSign,null,2));




// 6. Split the street address into it's component parts and store each as a new key in the object

const streetAddress = data.map((item) => {

    const captainAddress = item.captain.address.streetAddress.split(" ");
    const [captainStreetNumber, ...captainStreetName] = captainAddress;
    item.captain.streetNumber = captainStreetNumber;
    item.captain.streetName = captainStreetName.join(" ");

    const updatedPassengers = item.passengers.map((passenger) => {
        const passengerAddress = passenger.address.streetAddress.split(" ");
        const [passengerStreetNumber, ...passengerStreetName] = passengerAddress;
        passenger.streetNumber = passengerStreetNumber;
        passenger.streetName = passengerStreetName.join(" ");
        return passenger;
    });
    item.passengers = updatedPassengers;

    return item;
});

// console.log(JSON.stringify(streetAddress,null,2));



// 7. Group the data based on city using only one HOF


const groupedByCity = data.reduce((result, item) => {
    let city = item.captain.address.city;
    if (result.captains[city]) {
        result.captains[city].push(item.captain);
    } else {
        result.captains[city] = [item.captain]
    }

    item.passengers.reduce((passengerResult, passenger) => {
        let cities = passenger.address.city;
        if (passengerResult[cities]) {
            passengerResult[cities].push(passenger)
        } else {
            passengerResult[cities] = [passenger]
        }
        return passengerResult
    }, result.passengers)

    return result;

}, { captains: {}, passengers: {} })

console.log(JSON.stringify(groupedByCity, null, 2));


// 8. Group the data based on country using only one HOF

const groups = data.reduce((result, item) => {
    const country = item.captain.address.country;

    if (result.captains[country]) {
        result.captains[country].push(item.captain)
    } else {
        result.captains[country] = [item.captain];
    }

    item.passengers.reduce((passengerResult, passenger) => {
        let values = passenger.address.country;
        if (passengerResult[values]) {
            passengerResult[values].push(passenger)
        } else {
            passengerResult[values] = [passenger];
        }
        return passengerResult;
    }, result.passengers);

    return result;

}, { captains: {}, passengers: {} })

console.log(JSON.stringify(groups));


// 9. Convert the account number into the correct numeric equivalent.

let accountNumber = data.map((item, index) => {
    let captainAccountNumber = Number(item.captain.accountNumber);
    item.captain.accountNumber = captainAccountNumber;

    item.passengers.map((passenger, index) => {
        let passengerAccountNumber = Number(passenger.accountNumber);
        passenger.accountNumber = passengerAccountNumber;
        return passenger
    })

    return item;
})

console.log(JSON.stringify(accountNumber, null, 2));


// 10. Use the currency code key to make a more readable version with salary

let addCurrencyCodeKey = data.map((item, index) => {
    let captainSalary = `$${item.captain.salary}`;

    item.captain.salary = captainSalary;

    item.passengers.map((passenger, index) => {
        let passengerSalary = `$${passenger.salary}`;
        passenger.salary = passengerSalary;
        return passenger;
    })
    return item;

})

// console.log(JSON.stringify(addCurrencyCodeKey,null,2))



// 11. Find the total salary

const totalSalary = data.reduce((result, current) => {
    const captainSalary = parseFloat(current.captain.salary.substring(1));
    result.captainSalary += captainSalary;

    const passengersSalary = current.passengers.reduce((passengerResult, passenger) => {
        const passengerSalary = parseFloat(passenger.salary.substring(1));
        return passengerResult + passengerSalary;
    }, 0);
    result.passengerSalary += passengersSalary;

    return result;
}, { captainSalary: 0, passengerSalary: 0 });

// console.log(totalSalary);




//  12. Group the data based on experience using only one HOF

const experience = data.reduce((result, current) => {
    let captainExperience = current.captain.experience;
    if (result.captains[captainExperience]) {
        result.captains[captainExperience].push(current.captain);
    } else {
        result.captains[captainExperience] = [current.captain]
    }

    current.passengers.reduce((acc, passenger) => {
        let passengerExperience = passenger.experience;
        if (acc[passengerExperience]) {
            acc[passengerExperience].push(passenger)
        } else {
            acc[passengerExperience] = [passenger]
        }
        return acc;
    }, result.passengers)
    return result;

}, { captains: {}, passengers: {} })

// console.log(JSON.stringify(experience))


// 13. Flatten the currency object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.

// 14. Convert the salary into the correct numeric equivalent.

const salary = data.map((item, index) => {
    let captainSalary = Number(item.captain.salary.substring(1));

    item.captain.salary = captainSalary;

    item.passengers.map((passenger, index) => {
        let passengerSalary = Number(passenger.salary.substring(1));
        passenger.salary = passengerSalary;
        return passenger;
    })

    return item;

})

console.log(JSON.stringify(salary, null, 2))


// 15. Find the average salary grouped by experience



function calculateAverageSalaryByExperience(data) {
    const averageSalaryByExperience = data.reduce((result, item) => {
        const captainExperience = item.captain.experience;

        if (result.captains[captainExperience]) {
            result.captains[captainExperience].count++;
            result.captains[captainExperience].sum += Number(item.captain.salary);
            result.captains[captainExperience].averageSalary = 
            result.captains[captainExperience].sum / result.captains[captainExperience].count;
        } else {
            result.captains[captainExperience] =
            {
                count: 1,
                sum: Number(item.captain.salary),
                averageSalary: Number(item.captain.salary),
            }
        }
        item.passengers.reduce((acc, passenger) => {
            const passengerExperience = passenger.experience;
            if (acc[passengerExperience]) {
                acc[passengerExperience].count++;
                acc[passengerExperience].sum += Number(passenger.salary);
                acc[passengerExperience].averageSalary = acc[passengerExperience].sum / acc[passengerExperience].count;

            } else {
                acc[passengerExperience] = {
                    count: 1,
                    sum: Number(passenger.salary),
                    averageSalary: Number(passenger.salary)
                }
            }

            return acc;
        }, result.passengers);

        return result;
    }, { captains: {}, passengers: {} });
    return averageSalaryByExperience;

}

const averageSalaryByExperience = calculateAverageSalaryByExperience(data);
console.log(averageSalaryByExperience);


//   16. Flatten the airline object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.

function flattenAirlineData(data) {
    return data.map((item) => {
        const { airline, ...rest } = item;
        const mergedData = { ...rest, airlineName: airline.name, airlineIataCode: airline.iataCode };
        return mergedData;
    });
}

const flattened = flattenAirlineData(data);
console.log(JSON.stringify(flattened, null, 2));


//   17. Flatten the airplane object into the parent data. In case of conflicts, find a way to resolve it. If it cannot be resolved, avoid data loss.

// 18. Sort the data in descending order of code

const sortedDataOfCode = data.map((item) => {
    item.passengers = item.passengers.sort((item1, item2) => {
        if (item1.currency.code > item2.currency.code) {
            return -1;
        } else if (item1.currency.code < item2.currency.code) {
            return 1;
        } else {
            return 0;
        }
    })
    return item;

});

console.log(JSON.stringify(sortedDataOfCode, null, 2))


// 19. Use the experience as a factor to calculate the factored salary

const factoredSalary = data.map((item) => {
    const passengers = item.passengers.map((passenger) => ({
        ...passenger,
        factoredSalary: (parseFloat(passenger.salary) * parseInt(passenger.experience)).toFixed(2)
    }));

    const captain = {
        ...item.captain,
        factoredSalary: (parseFloat(item.captain.salary) * parseInt(item.captain.experience)).toFixed(2)
    };

    return {
        ...item,
        passengers,
        captain
    };
});

//   console.log(JSON.stringify(factoredSalary,null,2));


//   20. Sort the data in ascending order of name

const ascendingOrderOfName = data.sort((item1, item2) => {
    if (item1.airline.name > item2.airline.name) {
        return 1;
    } else if (item1.airline.name < item2.airline.name) {
        return 1;
    } else {
        return 0;
    }

})

console.log(JSON.stringify(ascendingOrderOfName, null, 2));




// 21. Find the average salary

// 22. Sort the data in descending order of iata type code

let iataCodeTypeDescendingOrder = data.sort((item1, item2) => {
    if (item1.airplane.iataTypeCode > item2.airplane.iataTypeCode) {
        return -1;
    } else if (item1.airplane.iataTypeCode < item2.airplane.iataTypeCode) {
        return 1;
    } else {
        return 0;
    }
});

// console.log(JSON.stringify(iataCodeTypeDescendingOrder))


// 23. Sort the data in descending order of iata code

let iataCodeDescendingOrder = data.sort((item1, item2) => {
    if (item1.airline.iataCode > item2.airline.iataCode) {
        return -1;
    } else if (item1.airline.iataCode < item2.airline.iataCode) {
        return 1;
    } else {
        return 0;
    }
});

console.log(JSON.stringify(iataCodeDescendingOrder));


// 24. Sort the data in ascending order of code

const sortedDataOfCodeInAscendingOrder = data.map((item) => {
    item.passengers = item.passengers.sort((item1, item2) => {
        if (item1.currency.code > item2.currency.code) {
            return 1;
        } else if (item1.currency.code < item2.currency.code) {
            return -1;
        } else {
            return 0;
        }
    })
    return item;

});

console.log(JSON.stringify(sortedDataOfCodeInAscendingOrder, null, 2))
